import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import pickle
from matplotlib.lines import Line2D

from main import Model

if __name__ == "__main__":

    """
    N = 100
    M = 100
    dat = np.random.binomial(1, 0.05, (M, N))
    #dat[10:20,10:20] = 1
    dat[10:50, 10:50] = np.random.binomial(1, 0.9, (40, 40))
    dat[40:70, 50:80] = np.random.binomial(1, 0.9, (30, 30))
    dat[80:90, 80:90] = np.random.binomial(1, 0.9, (10, 10))
    dat[70:90, 20:40] = np.random.binomial(1, 0.9, (20, 20))

    mask = np.zeros((M,N))
    #mask[10:20,10:20] = 1
    mask[10:50, 10:50] = 1
    mask[40:70, 50:80] = 2
    mask[80:90, 80:90] = 3
    mask[70:90, 20:40] = 4
    """

    with open('model.pkl', 'rb') as model:

        m = pickle.load(model)
        x = np.linspace(1, m.n, num=m.n)
        y = np.linspace(1, m.m, num=m.m)
        X, Y = np.meshgrid(x, y)

        dat = m.x
        tiles = m.getTiles()
        df_signal = pd.DataFrame({"y": Y.flatten(), "x": X.flatten(), "intensity": dat.flatten()})
        df_det = pd.DataFrame({"y": Y.flatten(), "x": X.flatten(), "tiles": tiles.flatten()})

        # prepare Dataframes
        df = df_signal.pivot(index="y", columns="x")
        dfmark = df_det[df_det["tiles"] > 0]

        # plotting


        fig, ax = plt.subplots()

        x = df_signal["x"].unique()
        y = df_signal["y"].unique()
        cmap = get_cmap("Greys")
        cmap.set_bad(color="red")
        ax.imshow(df,cmap=cmap,interpolation='nearest',origin="upper",aspect="equal")
        plt.tick_params(
            bottom=False,
            left=False,
            labelbottom=False,
            labelleft=False)

        cmap = get_cmap("Set1")
        colors = cmap.colors
        dx = np.diff(x)[0];
        dy = np.diff(y)[0]
        tlegend = np.sort(np.unique(dfmark["tiles"]).astype(int))
        custom_lines = [Line2D([0], [0], color='w',markerfacecolor=colors[x], marker='o',markersize=10) for x in tlegend]
        custom_labels = ["Tile " + str(x) for x in tlegend]
        ax.legend(custom_lines, custom_labels, loc="upper left", bbox_to_anchor=(1, 1))
        for (xi, yi, t), in zip(dfmark[["x", "y","tiles"]].values):
            print(xi)
            rec = plt.Rectangle((xi-1.5,yi-1.5), 1, 1, fill=False,
                                edgecolor=colors[int(t)], lw=0.5)
            ax.add_artist(rec)

        fig.savefig("test7.png",dpi=1000)

        plt.clf()
        # Histograms

        fig, ax = plt.subplots(m.t + 1,2)
        p, mp = m.getPosterior()
        for i in range(m.t+1):
            ax[i,0].hist(p[:,i],bins=20)
            ax[i,1].hist(mp[:,i],bins=20)
            if i == 0:
                ax[i, 0].set_ylabel("BG", rotation=0, labelpad=20, size='large')
                ax[i,0].set_title("P(1)")
                ax[i,1].set_title("P(Missing)")
            else:
                ax[i, 0].set_ylabel("Tile " + str(i), rotation=0, labelpad=20, size='large')

        plt.tight_layout()
        fig.savefig("test7hist.png",dpi=1000)
