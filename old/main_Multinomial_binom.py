import numpy as np
from scipy.misc import logsumexp
import pickle
from itertools import product
from scipy.special import comb

class Model():
    def __init__(
            self,
            x,
            t,
            a=1,
            b=1
    ):
        self.s = np.stack([r[0, :] for r in x])  # Success
        self.f = np.stack([np.sum(r[1:, :], axis=0) for r in x])  # Failures
        self.t = t # Number of tiles
        self.a = a # Alpha hyperparameter
        self.b = b # Beta hyperparameter
        self.n = self.s.shape[1]  # Number of samples
        self.m = self.s.shape[0]  # Number of features
        self.pc = np.repeat(0.5,self.t) # column prior
        self.pr = np.repeat(0.5,self.t) # row prior

        # Initialize random starts
        self.r = np.zeros((self.m, self.t))  # Row labels
        self.r[range(self.m), np.random.randint(0, self.t, self.m)] = 1
        self.c = np.zeros((self.n, self.t))  # Column labels
        self.c[range(self.n), np.random.randint(0, self.t, self.n)] = 1
        self.p = np.zeros(self.t + 1) # Tile value parameter
        self.mp = np.zeros(self.t + 1) # Tile missing value parameter
        self.ll = [np.zeros((self.m,self.n)) for i in range(self.t + 1)] # Likelihood mats
        self.pdist = None
        self.mpdist = None
        self.combos = [np.array(x) for x in product([0, 1], repeat=self.t)]

    def sampleR(self):
        zero = [np.outer(np.zeros(self.m), self.c[:, i]) for i in range(self.t)]
        one = [np.outer(np.ones(self.m), self.c[:, i]) for i in range(self.t)]
        zeroll = [zero[i] * self.ll[i+1] for i in range(self.t)]
        onell = [one[i] * self.ll[i+1] for i in range(self.t)]

        probs = np.zeros((2**self.t,self.m))
        mask = np.zeros((2**self.t,self.m))
        for i, idx in enumerate(self.combos):
            pos = np.zeros((self.m,self.n))
            ll = np.zeros((self.m,self.n))
            for j in range(self.t):
                if idx[j] == 1:
                    pos += one[j]
                    ll += onell[j]
                else:
                    pos += zero[j]
                    ll += zeroll[j]
            keep = 1 - np.any(pos > 1,axis=1)
            ll += self.ll[0] * (1-pos)
            ll = np.sum(ll,axis=1) + np.sum(np.log(self.pr * idx + (1 - self.pr) * (1 - idx)))
            mask[i,:] = keep
            probs[i,:] = ll
        probs = np.exp(probs - logsumexp(probs,b=mask,axis=0)) * mask
        probs[np.isnan(probs)] = 0
        for i in range(self.m):
            self.r[i,:] = self.combos[np.where(np.random.multinomial(1,probs[:,i]) == 1)[0][0]]

    def sampleC(self):
        zero = [np.outer(self.r[:,i], np.zeros(self.n)) for i in range(self.t)]
        one = [np.outer(self.r[:,i], np.ones(self.n)) for i in range(self.t)]
        zeroll = [zero[i] * self.ll[i+1] for i in range(self.t)]
        onell = [one[i] * self.ll[i+1] for i in range(self.t)]

        probs = np.zeros((2**self.t,self.n))
        mask = np.zeros((2**self.t,self.n))
        for i, idx in enumerate(self.combos):
            pos = np.zeros((self.m,self.n))
            ll = np.zeros((self.m,self.n))
            for j in range(self.t):
                if idx[j] == 1:
                    pos += one[j]
                    ll += onell[j]
                else:
                    pos += zero[j]
                    ll += zeroll[j]
            keep = 1 - np.any(pos > 1,axis=0)
            ll += self.ll[0] * (1-pos)
            ll = np.sum(ll,axis=0) + np.sum(np.log(self.pc * idx + (1 - self.pc) * (1 - idx)))
            mask[i,:] = keep
            probs[i,:] = ll
        probs = np.exp(probs - logsumexp(probs,b=mask,axis=0)) * mask
        probs[np.isnan(probs)] = 0
        for i in range(self.n):
            #print(np.sort(probs[:,1])[::-1][0:5])
            self.c[i,:] = self.combos[np.where(np.random.multinomial(1,probs[:,i]) == 1)[0][0]]


    def sampleP(self):
        tile_a = 0
        tile_b = 0
        tile_miss = 0
        tile_hit = 0
        alltiles = np.zeros((self.m,self.n))
        # Sample tile parameters
        for i in range(self.t):
            agree = np.outer(self.r[:,i],self.c[:,i])
            rows, cols = np.where(agree == 1)
            alpha = np.nansum(self.s[rows,cols])
            miss = np.sum(np.isnan(self.s[rows,cols]))
            beta = np.nansum(self.f[rows,cols])
            hit = len(self.s[rows,cols]) - miss
            self.p[i+1] = 0.9#np.random.beta(self.a + alpha,self.b + beta)
            #self.p[i+1] = np.random.choice(np.random.beta(self.a + self.s[rows,cols],self.b + self.f[rows,cols]))
            #self.mp[i+1] = np.random.beta(self.a + miss,self.b + hit)
            tile_a += alpha
            tile_b += beta
            tile_miss += miss
            tile_hit += hit
            alltiles += agree

        # Sample background parameters
        total_a = np.nansum(self.s)
        total_b = np.nansum(self.f)
        total_miss = np.sum(np.isnan(self.s))
        alpha = total_a - tile_a
        beta = total_b - tile_b
        alpha_miss = total_miss - tile_miss
        beta_miss = self.m * self.n - total_miss - tile_hit
        self.p[0] = np.random.beta(self.a + alpha, self.b + beta)
        #rows, cols = np.where(alltiles == 0)
        #self.p[0] = np.random.choice(np.random.beta(self.a + self.s[rows,cols],self.b + self.f[rows,cols]))
        #self.mp[0] = np.random.beta(self.a + alpha_miss, self.b + beta_miss)
        # Compute likelihood matrices and store
        for i in range(self.t + 1):
            self.ll[i] = np.log(self.p[i]) * self.s + np.log(1-self.p[i]) * self.f + np.log(comb(100,self.s))
            #missloc = np.isnan(self.s)
            #self.ll[i][missloc] = np.log(self.mp[i])
            #self.ll[i][~missloc] += np.log(1-self.mp[i])


    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.random.beta(self.a + alpha,self.b + self.n)

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.random.beta(self.b + alpha,self.a + self.m)


    def inference(self, iter=100000,burnin=50000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            self.sampleP()
            self.sampleC()
            self.sampleR()
            self.samplePC()
            self.samplePR()
            if i % thin == 0 and i >= burnin:
                self.pdist[cursamp,:] = self.p
                self.mpdist[cursamp,:] = self.mp
                cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist

if __name__ == "__main__":
    with open("simData.pkl","rb") as x:
        T = 4

        m = Model(x=pickle.load(x),t=T)
        m.inference(iter=6000,burnin=1500)

        #FIX
        out = m.getTiles()
        np.savetxt("test.txt",out,fmt='%i',delimiter=" ")
        print(m.getTileParameters())
        print(m.getTileSize())
        print(m.c)
        print(m.r)
        with open("model.pkl","wb") as model:
            pickle.dump(m,model)



