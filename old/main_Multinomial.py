import numpy as np
from scipy.misc import logsumexp
import pickle
from itertools import product

class Model():
    def __init__(
            self,
            x,
            t,
            s=5,
            a=1,
            b=1
    ):
        self.x = x # Data
        self.t = t # Number of tiles
        self.a = a # Alpha hyperparameter
        self.b = b # Beta hyperparameter
        self.s = s # Minimum tile size
        self.n = self.x.shape[1] # Number of samples
        self.m = self.x.shape[0] # Number of features
        self.pc = np.repeat(0.5,self.t) # column prior
        self.pr = np.repeat(0.5,self.t) # row prior

        # Initialize random starts
        self.r = np.zeros((self.m, self.t))  # Row labels
        self.r[range(self.m), np.random.randint(0, self.t, self.m)] = 1
        self.c = np.zeros((self.n, self.t))  # Column labels
        self.c[range(self.n), np.random.randint(0, self.t, self.n)] = 1
        self.p = np.zeros(self.t + 1) # Tile value parameter
        self.mp = np.zeros(self.t + 1) # Tile missing value parameter
        self.ll = [np.zeros((self.m,self.n)) for i in range(self.t + 1)] # Likelihood mats
        self.pdist = None
        self.mpdist = None
        self.combos = [np.array(x) for x in product([0, 1], repeat=self.t)]

    def sampleR(self):
        zero = [np.outer(np.zeros(self.m), self.c[:, i]) for i in range(self.t)]
        one = [np.outer(np.ones(self.m), self.c[:, i]) for i in range(self.t)]
        zeroll = [zero[i] * self.ll[i+1] for i in range(self.t)]
        onell = [one[i] * self.ll[i+1] for i in range(self.t)]

        probs = np.zeros((2**self.t,self.m))
        mask = np.zeros((2**self.t,self.m))
        for i, idx in enumerate(self.combos):
            pos = np.zeros((self.m,self.n))
            ll = np.zeros((self.m,self.n))
            for j in range(self.t):
                if idx[j] == 1:
                    pos += one[j]
                    ll += onell[j]
                else:
                    pos += zero[j]
                    ll += zeroll[j]
            keep = 1 - np.any(pos > 1,axis=1)
            ll += self.ll[0] * (1-pos)
            ll = np.sum(ll,axis=1) + np.sum(np.log(self.pr * idx + (1 - self.pr) * (1 - idx)))
            mask[i,:] = keep
            probs[i,:] = ll
        probs = np.exp(probs - logsumexp(probs,b=mask,axis=0)) * mask
        for i in range(self.m):
            self.r[i,:] = self.combos[np.where(np.random.multinomial(1,probs[:,i]) == 1)[0][0]]

    def sampleC(self):
        zero = [np.outer(self.r[:,i], np.zeros(self.n)) for i in range(self.t)]
        one = [np.outer(self.r[:,i], np.ones(self.n)) for i in range(self.t)]
        zeroll = [zero[i] * self.ll[i+1] for i in range(self.t)]
        onell = [one[i] * self.ll[i+1] for i in range(self.t)]

        probs = np.zeros((2**self.t,self.n))
        mask = np.zeros((2**self.t,self.n))
        for i, idx in enumerate(self.combos):
            pos = np.zeros((self.m,self.n))
            ll = np.zeros((self.m,self.n))
            for j in range(self.t):
                if idx[j] == 1:
                    pos += one[j]
                    ll += onell[j]
                else:
                    pos += zero[j]
                    ll += zeroll[j]
            keep = 1 - np.any(pos > 1,axis=0)
            ll += self.ll[0] * (1-pos)
            ll = np.sum(ll,axis=0) + np.sum(np.log(self.pc * idx + (1 - self.pc) * (1 - idx)))
            mask[i,:] = keep
            probs[i,:] = ll

        probs = np.exp(probs - logsumexp(probs,b=mask,axis=0)) * mask
        for i in range(self.n):
            self.c[i,:] = self.combos[np.where(np.random.multinomial(1,probs[:,i]) == 1)[0][0]]


    def sampleP(self):
        allrows = []
        allcols = []
        # Sample tile parameters
        for i in range(self.t):
            agree = np.outer(self.r[:,i],self.c[:,i])
            rows, cols = np.where(agree == 1)
            total = len(self.x[rows,cols])
            alpha = np.nansum(self.x[rows,cols])
            miss = np.sum(np.isnan(self.x[rows,cols]))
            beta = total - miss - alpha
            self.p[i+1] = np.random.beta(self.a + alpha,self.b + beta)
            self.mp[i+1] = np.random.beta(self.a + miss,self.b + total)
            allrows.append(rows)
            allcols.append(cols)
        allrows = np.concatenate(allrows)
        allcols = np.concatenate(allcols)

        # Sample background parameters
        msk = np.ma.array(self.x,mask=False)
        msk.mask[allrows,allcols] = True
        total = msk.count()
        missloc = np.isnan(msk)
        miss = np.sum(missloc)
        msk.mask[missloc] = True
        alpha = msk.sum()
        beta = msk.count() - alpha
        self.p[0] = np.random.beta(self.a + alpha, self.b + beta)
        self.mp[0] = np.random.beta(self.a + miss, self.b + total)

        # Compute likelihood matrices and store
        for i in range(self.t + 1):
            self.ll[i][:,:] = np.log(self.p[i])
            self.ll[i][np.where(self.x == 0)] = np.log(1 - self.p[i])
            missloc = np.isnan(self.x)
            self.ll[i][missloc] = np.log(self.mp[i])
            self.ll[i][~missloc] += np.log(1-self.mp[i])

    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.random.beta(self.a + alpha,self.b + self.n)

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.random.beta(self.b + alpha,self.a + self.m)


    def inference(self, iter=100000,burnin=50000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            self.sampleP()
            self.sampleC()
            self.sampleR()
            self.samplePC()
            self.samplePR()
            if i % thin == 0 and i >= burnin:
                self.pdist[cursamp,:] = self.p
                self.mpdist[cursamp,:] = self.mp
                cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist

if __name__ == "__main__":
    S = 5
    T = 4
    N = 90
    M = 100
    x = np.random.binomial(1,0.05,(M,N)).astype(float)
    #x[np.random.randint(0,M,20),np.random.randint(0,N,20)] = np.nan
    x[10:50,10:50] = np.random.binomial(1,0.9,(40,40)).astype(float)
    #x[np.random.randint(10,50,400),np.random.randint(10,50,400)] = np.nan
    x[40:70,50:80] = np.random.binomial(1,0.9,(30,30)).astype(float)
    #x[np.random.randint(40,70,10),np.random.randint(50,80,10)] = np.nan
    x[80:90,80:90] = np.random.binomial(1,0.9,(10,10)).astype(float)
    #x[np.random.randint(80,90,70),np.random.randint(80,90,70)] = np.nan
    x[70:90,20:40] = np.random.binomial(1,0.9,(20,20)).astype(float)
    #x[np.random.randint(70,90,30),np.random.randint(20,40,30)] = np.nan

    m = Model(x=x,t=T,s=S)
    m.inference(iter=30000,burnin=15000,thin=100)
    out = m.getTiles()
    np.savetxt("test.txt",out,fmt='%i',delimiter=" ")
    print(m.getTileParameters())
    print(m.getTileSize())
    with open("model.pkl","wb") as model:
        pickle.dump(m,model)



