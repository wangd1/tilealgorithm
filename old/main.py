import numpy as np
from scipy.misc import logsumexp
import pickle

class Model():
    def __init__(
            self,
            x,
            t,
            s=5,
            a=1,
            b=1
    ):
        self.x = x # Data
        self.t = t # Number of tiles
        self.a = a # Alpha hyperparameter
        self.b = b # Beta hyperparameter
        self.s = s # Minimum tile size
        self.n = self.x.shape[1] # Number of samples
        self.m = self.x.shape[0] # Number of features
        self.pc = np.repeat(np.log(0.5),self.t) # column prior
        self.pr = np.repeat(np.log(0.5),self.t) # row prior

        # Initialize random starts
        self.r = np.zeros((self.m,self.t)) # Row labels
        self.r[range(self.m),np.random.randint(0,self.t,self.m)] = 1
        self.c = np.zeros((self.n,self.t)) # Column labels
        self.r[range(self.n),np.random.randint(0,self.t,self.n)] = 1
        self.p = np.zeros(self.t + 1) # Tile value parameter
        self.mp = np.zeros(self.t + 1) # Tile missing value parameter
        self.ll = [np.zeros((self.m,self.n)) for i in range(self.t + 1)] # Likelihood mats
        self.pdist = None
        self.mpdist = None

    def sampleR(self):
        # Precompute some outer products for constraints
        #op = [np.outer(self.r[:,i],self.c[:,i]) for i in range(self.t)]
        try:
            for i in range(self.t):
                ll = np.sum(self.ll[i+1] * self.c[:,i],axis=1) + self.pr[i]
                bg = np.sum(self.ll[0] * self.c[:,i],axis=1) + (1-self.pr[i])
                probs = np.exp(ll - logsumexp([ll,bg],axis=0))

                # Enforce constraint
                #constraint = np.outer(np.ones(self.m),self.c[:,i]) + np.sum([x for j, x in enumerate(op) if j != i],axis=0)
                constraint = [np.outer(self.r[:,j],self.c[:,j]) if i != j else np.outer(np.ones(self.m),self.c[:,j]) for j in range(self.t)]
                constraint = np.sum(constraint,axis=0)
                remove = np.any(constraint > 1,axis=1)
                probs[remove] = 0
                self.r[:,i] = np.random.binomial(1,probs)

                # Enforce minimum tile size
                # May need to check for non zero probs
                #if np.sum(self.r[:,i]) < self.s:
                    #self.r[:,i] = 0
                    #self.r[np.argsort(probs)[len(probs)-self.s:],i] = 1
        except Warning:
            print("end")

    def sampleC(self):
        #op = [np.outer(self.r[:,i],self.c[:,i]) for i in range(self.t)]
        try:
            for i in range(self.t):
                ll = np.sum(self.ll[i+1].T * self.r[:,i],axis=1) + self.pc[i]
                bg = np.sum(self.ll[0].T * self.r[:,i],axis=1) + (1-self.pc[i])
                probs = np.exp(ll - logsumexp([ll,bg],axis=0))

                # Enforce constraint
                #constraint = np.outer(self.r[:, i],np.ones(self.n)) + np.sum([x for j, x in enumerate(op) if j != i],axis=0)
                constraint = [np.outer(self.r[:, j], self.c[:, j]) if i != j else np.outer(self.r[:, j],np.ones(self.n)) for j in range(self.t)]
                constraint = np.sum(constraint, axis=0)
                remove = np.any(constraint > 1, axis=0)
                probs[remove] = 0

                self.c[:,i] = np.random.binomial(1,probs)

                # Enforce minimum tile size
                #if np.sum(self.c[:,i]) < self.s:
                #    self.c[:,i] = 0
                #    self.c[np.argsort(probs)[len(probs)-self.s:],i] = 1
        except Warning:
            print("end")

    def sampleP(self):
        allrows = []
        allcols = []
        # Sample tile parameters
        for i in range(self.t):
            agree = np.outer(self.r[:,i],self.c[:,i])
            rows, cols = np.where(agree == 1)
            total = len(self.x[rows,cols])
            alpha = np.nansum(self.x[rows,cols])
            miss = np.sum(np.isnan(self.x[rows,cols]))
            beta = total - miss - alpha
            self.p[i+1] = np.random.beta(self.a + alpha,self.b + beta)
            self.mp[i+1] = np.random.beta(self.a + miss,self.b + total)
            allrows.append(rows)
            allcols.append(cols)
        allrows = np.concatenate(allrows)
        allcols = np.concatenate(allcols)

        # Sample background parameters
        msk = np.ma.array(self.x,mask=False)
        msk.mask[allrows,allcols] = True
        total = msk.count()
        missloc = np.isnan(msk)
        miss = np.sum(missloc)
        msk.mask[missloc] = True
        alpha = msk.sum()
        beta = msk.count() - alpha
        self.p[0] = np.random.beta(self.a + alpha, self.b + beta)
        self.mp[0] = np.random.beta(self.a + miss, self.b + total)

        # Compute likelihood matrices and store
        for i in range(self.t + 1):
            self.ll[i][:,:] = np.log(self.p[i])
            self.ll[i][np.where(self.x == 0)] = np.log(1 - self.p[i])
            missloc = np.isnan(self.x)
            self.ll[i][missloc] = np.log(self.mp[i])
            self.ll[i][~missloc] += np.log(1-self.mp[i])

    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.log(np.random.beta(10 + alpha,10 + self.n))

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.log(np.random.beta(10 + alpha,10 + self.m))


    def inference(self, iter=30000,burnin=15000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            self.sampleP()
            self.sampleR()
            self.sampleC()
            self.samplePC()
            self.samplePR()
            if i % thin == 0 and i >= burnin:
                self.pdist[cursamp,:] = self.p
                self.mpdist[cursamp,:] = self.mp
                cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist

if __name__ == "__main__":
    S = 2
    T = 2
    N = 90
    M = 100
    """
    x = np.random.binomial(1,0.05,(M,N)).astype(float)
    x[np.random.randint(0,M,20),np.random.randint(0,N,20)] = np.nan
    x[10:50,10:50] = np.random.binomial(1,0.9,(40,40)).astype(float)
    #x[np.random.randint(10,50,400),np.random.randint(10,50,400)] = np.nan
    x[40:70,50:80] = np.random.binomial(1,0.5,(30,30)).astype(float)
    #x[np.random.randint(40,70,10),np.random.randint(50,80,10)] = np.nan
    x[80:90,80:90] = np.random.binomial(1,0.9,(10,10)).astype(float)
    #x[np.random.randint(80,90,70),np.random.randint(80,90,70)] = np.nan
    x[70:90,20:40] = np.random.binomial(1,0.90,(20,20)).astype(float)
    #x[np.random.randint(70,90,30),np.random.randint(20,40,30)] = np.nan
    """
    x = np.zeros((100, 90))
    x[10:40, 10:40] = np.random.binomial(1, 1, (30, 30)).astype(float)
    x[50:80, 50:80] = np.random.binomial(1, 1, (30, 30)).astype(float)

    m = Model(x=x,t=T,s=S)
    m.inference(iter=6000,burnin=1500)
    out = m.getTiles()
    np.savetxt("test.txt",out,fmt='%i',delimiter=" ")
    print(m.getTileParameters())
    print(m.getTileSize())
    with open("model.pkl","wb") as model:
        pickle.dump(m,model)



