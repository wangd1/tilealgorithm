import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import pickle

if __name__ == "__main__":
    n_row = 70
    n_col = 80
    bn_row = np.repeat(100,n_row)#np.random.poisson(50,n_row)
    tile = np.zeros((n_row,n_col),int)
    tile[0:20,0:20] = 1
    tile[10:40,40:70] = 2
    tile[50:70,60:80] = 3
    tile[30:60,10:40] = 4
    max_lsv_len = 2

    tile_p = [0.05,0.9,0.3,0.5,0.7]
    tile_mp = [0.0,0.2,0.2,0.3,0.4]
    dat = np.zeros((max_lsv_len, n_row, n_col), float)

    for i in range(n_row):
        lsv_len = 2#np.random.choice([2,3,4],size=1,p=[0.7,0.2,0.1])[0] #np.random.randint(2,max_lsv_len+1)
        rep_pos = 0#np.random.randint(0, lsv_len)
        #bg_dist = np.random.dirichlet(np.repeat(1,lsv_len),1)[0]
        bg_psi_rep = np.random.beta(0.5,0.5)#np.random.beta(1/lsv_len,(lsv_len - 1) / lsv_len)
        bg_psi_bg = np.random.dirichlet(np.repeat(1,lsv_len-1))
        bg_psi = np.append(bg_psi_rep,(1-bg_psi_rep) / np.sum(bg_psi_bg) * bg_psi_bg)
        #bg_dist = np.array([bg_psi,1-bg_psi])
        for j in range(n_col):
            if np.random.binomial(1,tile_mp[tile[i,j]]):
                dat[:,i,j] = np.nan
            else:

                if tile[i,j] > 0:
                    tile_psi = bg_psi.copy()
                    tile_psi = np.random.dirichlet(np.repeat(1,lsv_len))
                    tile_psi[rep_pos] = 0
                    tile_psi = (1-tile_p[tile[i,j]]) / np.sum(tile_psi) * tile_psi
                    tile_psi[rep_pos] = tile_p[tile[i,j]]
                    dat[0:lsv_len,i,j] = np.random.multinomial(bn_row[i],tile_psi)
                else:

                    """
                    s = bg_psi
                    f = 1 - bg_psi
                    smaller = np.minimum(s,f)
                    noise = np.random.normal(0,smaller/3)
                    if noise < 0:
                        noise = 0
                    if noise > smaller:
                        noise = smaller
                    #if bg_psi - noise > 0 and bg_psi + noise < 1:
                    bg_dist = np.array([bg_psi - noise, 1 - bg_psi + noise])
                    bg_psi = np.random.beta(0.5, 0.5)
                    bg_dist = np.array([bg_psi, 1 - bg_psi])
                    print(bg_dist)
                    #np.random.shuffle(bg_dist)
                    dat[:,i,j] = np.random.multinomial(bn_row[i],bg_dist)
                    """
                    noise = np.abs(np.random.normal(0,0.2,lsv_len))
                    noise_psi = bg_psi + noise
                    new_psi = noise_psi * (1/np.sum(noise_psi))
                    print(new_psi)
                    #new_psi = [0.05,0.95]
                    #new_psi = np.random.dirichlet(np.repeat(1,lsv_len))
                    dat[0:lsv_len,i,j] = np.random.multinomial(bn_row[i],new_psi)

                # Move density
                #remainder = 1 - tile_p[tile[i,j]]
                #idx = np.arange(lsv_len) != rep_pos
                #scale = remainder / np.sum(bg_dist[idx])
                #tmp = bg_dist.copy()
                #tmp[rep_pos] = tile_p[tile[i,j]]
                #tmp[idx] = tmp[idx] * scale
                #print(tmp)
                #dat[np.arange(lsv_len), i, j] = np.random.multinomial(bn_row[i],tmp)

    row_shuffle = np.arange(dat.shape[1])
    np.random.shuffle(row_shuffle)
    col_shuffle = np.arange(dat.shape[2])
    np.random.shuffle(col_shuffle)
    #dat = dat[:,row_shuffle,:]
    #dat = dat[:,:,col_shuffle]




    with open("simData.pkl", "wb") as d:
        pickle.dump(dat, d)

    # Vis

    x = np.linspace(1, n_col, num=n_col)
    y = np.linspace(1, n_row, num=n_row)
    X, Y = np.meshgrid(x, y)

    data = (dat[0,:,:].T / bn_row[i]).T
    df_signal = pd.DataFrame({"y": Y.flatten(), "x": X.flatten(), "intensity": data.flatten()})

    # prepare Dataframes
    df = df_signal.pivot(index="y", columns="x")

    # plotting


    fig, ax = plt.subplots()

    x = df_signal["x"].unique()
    y = df_signal["y"].unique()
    cmap = get_cmap("Greys")
    cmap.set_bad(color="red")
    ax.imshow(df,cmap=cmap,interpolation='nearest',origin="upper",aspect="equal")
    plt.tick_params(
        bottom=False,
        left=False,
        labelbottom=False,
        labelleft=False)

    cmap = get_cmap("Set1")
    colors = cmap.colors
    dx = np.diff(x)[0];
    dy = np.diff(y)[0]

    fig.savefig("sim5.png",dpi=1000)