from scipy.special import gammaln
import numpy as np
from scipy.special import beta
from scipy.misc import comb

def dbetabinom(n,k,a,b,nanzero=True):
    ll = gammaln(n+1) + gammaln(k+a) + gammaln(n-k+b) + gammaln(a+b) - \
           (gammaln(k+1) + gammaln(n-k+1) + gammaln(a) + gammaln(b) + gammaln(n+a+b))
    if nanzero:
        ll[np.where(np.isnan(ll))[0]] = 0.0
    return ll

def MH_proposal(prev,p):
    proposal = prev.copy()
    r = np.random.rand(prev.shape[0])
    gen_trunc = np.floor(np.log(1 - r * (1 - (1 - p) ** prev.shape[1])) / np.log(1 - p)).astype(int) + 1
    for i, x in enumerate(gen_trunc):
        idx = np.random.randint(0,prev.shape[1],x)
        proposal[i, idx] = 1 - proposal[i, idx]
    return proposal

if __name__ == "__main__":
    #print(dbetabinom(np.log(comb(100,100)),100,100,0.5,0.5))
    print(dbetabinom(100,50,0.5,0.5))