import numpy as np
from scipy.misc import logsumexp
from scipy.misc import comb
from util import dbetabinom
from util import MH_proposal
import pickle

class Model():
    def __init__(
            self,
            x,
            t,
            a=1,
            b=1
    ):
        self.x = x # Data
        self.n = x.shape[2]  # Number of samples
        self.m = x.shape[1]  # Number of features
        self.maxJ = x.shape[0] # Maximum number of junctions
        self.totalReads = np.sum(self.x,axis=0) # Total reads
        self.s = self.x[0].copy() # Success matrix
        self.f = self.totalReads - self.s # Failure matrix
        self.miss = np.isnan(self.s).astype(int) # Missing values
        self.nonmiss = 1 - self.miss # Non missing
        self.t = t # Number of tiles
        self.MH_tuning = 0.5 # MH tuning parameter

        # Initialize starts
        self.r = np.zeros((self.m, self.t),int)  # Row labels
        self.c = np.zeros((self.n,self.t),int) # Column labels
        row_idx = np.repeat(np.arange(self.t),int(self.m / self.t))
        col_idx = np.repeat(np.arange(self.t),int(self.n/ self.t))
        self.r[np.arange(len(row_idx)),row_idx] = 1
        self.c[np.arange(len(col_idx)),col_idx] = 1
        self.a = np.array([a] + [np.nansum(np.outer(self.r[:, i], self.c[:, i]) * self.s) + a for i in range(self.t)])
        self.b = np.array([b] + [np.nansum(np.outer(self.r[:, i], self.c[:, i]) * self.f) + b for i in range(self.t)])
        self.miss_a = np.array([a] + [np.sum(np.outer(self.r[:, i], self.c[:, i]) * self.miss) + a for i in range(self.t)])
        self.miss_b = np.array([b] + [np.sum(np.outer(self.r[:, i], self.c[:, i]) * self.nonmiss) + b for i in range(self.t)])

    def sampleR(self):
        proposal = MH_proposal(self.r,self.MH_tuning)

        row_labels_cur = [np.outer(proposal[:,i],self.c[:,i]) for i in range(self.t)]
        row_labels_prev = [np.outer(self.r[:,i],self.c[:,i]) for i in range(self.t)]
        alpha_cur = np.empty((self.m,self.t))
        beta_cur = np.empty((self.m,self.t))
        alpha_prev = np.empty((self.m,self.t))
        beta_prev = np.empty((self.m,self.t))
        miss_alpha_cur = np.empty((self.m,self.t),int)
        miss_beta_cur = np.empty((self.m,self.t),int)
        miss_alpha_prev = np.empty((self.m,self.t),int)
        miss_beta_prev = np.empty((self.m,self.t),int)
        for i in range(self.t):
            alpha_cur[:,i] = np.nansum(row_labels_cur[i] * self.s,axis=1)
            beta_cur[:,i] = np.nansum(row_labels_cur[i] * self.f,axis=1)
            alpha_prev[:,i] = np.nansum(row_labels_prev[i] * self.s,axis=1)
            beta_prev[:,i] = np.nansum(row_labels_prev[i] * self.f,axis=1)
            miss_alpha_cur[:,i] = np.nansum(row_labels_cur[i] * self.miss,axis=1)
            miss_beta_cur[:,i] = np.nansum(row_labels_cur[i] * self.nonmiss,axis=1)
            miss_alpha_prev[:,i] = np.nansum(row_labels_prev[i] * self.miss, axis=1)
            miss_beta_prev[:,i] = np.nansum(row_labels_prev[i] * self.nonmiss, axis=1)

        for i in range(self.m):
            self.a[1:] -= alpha_prev[i,:]
            self.b[1:] -= beta_prev[i,:]
            self.miss_a[1:] -= miss_alpha_prev[i,:]
            self.miss_b[1:] -= miss_beta_prev[i,:]
            prevll = np.zeros(self.n)
            curll = np.zeros(self.n)
            total_prev = np.zeros(self.n)
            total_cur = np.zeros(self.n)
            for j in range(self.t):
                idx_prev = np.where(row_labels_prev[j][i,:] > 0)[0]
                idx_cur = np.where(row_labels_cur[j][i,:] > 0)[0]
                prevll[idx_prev] += dbetabinom(self.totalReads[i,idx_prev],self.s[i,idx_prev],self.a[j+1],self.b[j+1])
                prevll[idx_prev] += dbetabinom(1,self.miss[i,idx_prev],self.miss_a[j+1],self.miss_b[j+1])
                curll[idx_cur] += dbetabinom(self.totalReads[i,idx_cur],self.s[i,idx_cur],self.a[j+1],self.b[j+1])
                curll[idx_cur] += dbetabinom(1,self.miss[i,idx_cur],self.miss_a[j+1],self.miss_b[j+1])
                total_prev[idx_prev] += 1
                total_cur[idx_cur] += 1
            bg_prev = np.where(prevll == 0)[0]
            bg_cur = np.where(curll == 0)[0]
            prevll[bg_prev] += dbetabinom(self.totalReads[i,bg_prev],self.s[i,bg_prev],self.a[0],self.b[0])
            prevll[bg_prev] += dbetabinom(1,self.miss[i,bg_prev],self.miss_a[0],self.miss_b[0])
            curll[bg_cur] += dbetabinom(self.totalReads[i,bg_cur],self.s[i,bg_cur],self.a[0],self.b[0])
            curll[bg_cur] += dbetabinom(1,self.miss[i,bg_cur],self.miss_a[0],self.miss_b[0])
            total_prev[bg_prev] += 1
            total_cur[bg_cur] += 1
            prevll = np.sum(prevll / total_prev)
            curll = np.sum(curll / total_cur)

            ratio = np.minimum(0, curll - prevll)
            u = np.log(np.random.uniform(0,1))
            if u < ratio:
                self.r[i,:] = proposal[i,:]
                self.a[1:] += alpha_cur[i,:]
                self.b[1:] += beta_cur[i,:]
                self.miss_a[1:] += miss_alpha_cur[i,:]
                self.miss_b[1:] += miss_beta_cur[i,:]
            else:
                self.a[1:] += alpha_prev[i,:]
                self.b[1:] += beta_prev[i,:]
                self.miss_a[1:] += miss_alpha_prev[i,:]
                self.miss_b[1:] += miss_beta_prev[i,:]


    def sampleC(self):
        proposal = MH_proposal(self.c,self.MH_tuning)
        col_labels_cur = [np.outer(self.r[:,i],proposal[:,i]) for i in range(self.t)]
        col_labels_prev = [np.outer(self.r[:,i],self.c[:,i]) for i in range(self.t)]
        alpha_cur = np.empty((self.n,self.t))
        beta_cur = np.empty((self.n,self.t))
        alpha_prev = np.empty((self.n,self.t))
        beta_prev = np.empty((self.n,self.t))
        miss_alpha_cur = np.empty((self.n,self.t),int)
        miss_beta_cur = np.empty((self.n,self.t),int)
        miss_alpha_prev = np.empty((self.n,self.t),int)
        miss_beta_prev = np.empty((self.n,self.t),int)

        for i in range(self.t):
            alpha_cur[:,i] = np.nansum(col_labels_cur[i] * self.s,axis=0)
            beta_cur[:,i] = np.nansum(col_labels_cur[i] * self.f,axis=0)
            alpha_prev[:,i] = np.nansum(col_labels_prev[i] * self.s,axis=0)
            beta_prev[:,i] = np.nansum(col_labels_prev[i] * self.f,axis=0)
            miss_alpha_cur[:,i] = np.nansum(col_labels_cur[i] * self.miss,axis=0)
            miss_beta_cur[:,i] = np.nansum(col_labels_cur[i] * self.nonmiss,axis=0)
            miss_alpha_prev[:,i] = np.nansum(col_labels_prev[i] * self.miss, axis=0)
            miss_beta_prev[:,i] = np.nansum(col_labels_prev[i] * self.nonmiss, axis=0)

        for i in range(self.n):
            self.a[1:] -= alpha_prev[i,:]
            self.b[1:] -= beta_prev[i,:]
            self.miss_a[1:] -= miss_alpha_prev[i, :]
            self.miss_b[1:] -= miss_beta_prev[i, :]
            prevll = np.zeros(self.m)
            curll = np.zeros(self.m)
            total_prev = np.zeros(self.m)
            total_cur = np.zeros(self.m)
            for j in range(self.t):
                idx_prev = np.where(col_labels_prev[j][:,i] > 0)[0]
                idx_cur = np.where(col_labels_cur[j][:,i] > 0)[0]
                prevll[idx_prev] += dbetabinom(self.totalReads[idx_prev,i], self.s[idx_prev, i], self.a[j+1],self.b[j+1])
                prevll[idx_prev] += dbetabinom(1, self.miss[idx_prev, i], self.miss_a[j+1],self.miss_b[j+1])
                curll[idx_cur] += dbetabinom(self.totalReads[idx_cur,i], self.s[idx_cur,i], self.a[j+1],self.b[j+1])
                curll[idx_cur] += dbetabinom(1, self.miss[idx_cur,i], self.miss_a[j+1],self.miss_b[j+1])
                total_prev[idx_prev] += 1
                total_cur[idx_cur] += 1
            bg_prev = np.where(prevll == 0)[0]
            bg_cur = np.where(curll == 0)[0]
            prevll[bg_prev] += dbetabinom(self.totalReads[bg_prev,i], self.s[bg_prev,i], self.a[0], self.b[0])
            prevll[bg_prev] += dbetabinom(1, self.miss[bg_prev,i], self.miss_a[0], self.miss_b[0])
            curll[bg_cur] += dbetabinom(self.totalReads[bg_cur,i], self.s[bg_cur,i], self.a[0], self.b[0])
            curll[bg_cur] += dbetabinom(1, self.miss[bg_cur,i], self.miss_a[0], self.miss_b[0])
            total_prev[bg_prev] += 1
            total_cur[bg_cur] += 1
            prevll = np.sum(prevll / total_prev)
            curll = np.sum(curll / total_cur)
            ratio = np.minimum(0, curll - prevll)
            u = np.log(np.random.uniform(0,1))
            if u < ratio:
                self.c[i,:] = proposal[i,:]
                self.a[1:] += alpha_cur[i,:]
                self.b[1:] += beta_cur[i,:]
                self.miss_a[1:] += miss_alpha_cur[i,:]
                self.miss_b[1:] += miss_beta_cur[i,:]
            else:
                self.a[1:] += alpha_prev[i,:]
                self.b[1:] += beta_prev[i,:]
                self.miss_a[1:] += miss_alpha_prev[i,:]
                self.miss_b[1:] += miss_beta_prev[i,:]


    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.random.beta(10 + alpha,10 + self.n)

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.random.beta(10 + alpha,10 + self.m)

    def sampleJ(self):
        labels = [np.outer(self.r[:, i], self.c[:, i]) for i in range(self.t)]
        for i in range(self.m):
            for j in range(self.t):
                self.a[j+1] -= np.nansum(labels[j][i,:] * self.s[i,:])
                self.b[j+1] -= np.nansum(labels[j][i,:] * self.f[i,:])

            llJ = np.zeros(self.maxJ)
            for k in range(self.maxJ):
                if np.nansum(self.x[k,i,:]) > 0:
                    ll = np.zeros(self.n)
                    total = np.zeros(self.n)
                    for j in range(self.t):
                        idx = np.where(labels[j][i,:] > 0)[0]
                        ll[idx] += dbetabinom(self.totalReads[i,idx], self.x[k,i,idx], self.a[j + 1],self.b[j + 1],nanzero=False)
                        total[idx] += 1
                    bg = np.where(ll == 0)[0]
                    ll[bg] = dbetabinom(self.totalReads[i,bg], self.x[k,i,bg], self.a[0], self.b[0],nanzero=False)
                    total[bg] += 1
                    llJ[k] = np.nansum(ll / total)
                else:
                    llJ[k] = -np.inf
            probs = np.exp(llJ - logsumexp(llJ))
            flip = np.where(np.random.multinomial(1,probs))[0]
            self.s[i,:] = self.x[flip,i,:]
            self.f[i,:] = self.totalReads[i,:] - self.x[flip,i,:]
            for j in range(self.t):
                self.a[j+1] += np.nansum(labels[j][i,:] * self.s[i,:])
                self.b[j+1] += np.nansum(labels[j][i,:] * self.f[i,:])

    def inference(self, iter=30000,burnin=15000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            #if i % 1000 == 0:
            print(i)
            #self.sampleP()
            self.sampleC()
            self.sampleR()
            #elf.samplePC()
            #self.samplePR()
            self.sampleJ()
            #if i % thin == 0 and i >= burnin:
                #self.pdist[cursamp,:] = self.p
                #self.mpdist[cursamp,:] = self.mp
                #cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist







