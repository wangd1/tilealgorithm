import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import pickle
from matplotlib.lines import Line2D
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
from tspy import TSP
from tspy.solvers import TwoOpt_solver

with open('model.pkl', 'rb') as model:
    m = pickle.load(model)
    print(m.x[0,1,:])
    print(m.x[1,1, :])
    print(m.x[2,1, :])
    print(m.x[3,1, :])