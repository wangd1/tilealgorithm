import argparse
import warnings

VER = 1.0
DEF_ITER = 20000
DEF_BURNIN = 10000
DEF_THIN = 100

def cmdline_args():
    # Make parser object
    p = argparse.ArgumentParser(prog="CHESSBOARD",
                                description="A program for finding tiles in RNA seq datasets.")
    p.add_argument("data",
                   help="Pickle file of input splicing data.")
    p.add_argument("T", type=int,
                   help="Number of tiles.")
    p.add_argument("output",
                   help="Output prefix.")
    p.add_argument("-i", "--iter", type=int, default=DEF_ITER,
                   help="Max number of iterations to run sampler.")
    p.add_argument("-b", "--burnin", type=int, default=DEF_BURNIN,
                   help="Burn-in iterations.")
    p.add_argument("-t", "--thin", type=int, default=DEF_THIN,
                   help="Thinning parameter.")
    p.add_argument("-p", "--plot", action="store_true",
                   help="Plot the result.")
    p.add_argument("--version", action="version", version="%(prog)s v{}".format(VER),
                   help="Print software version.")
    p.add_argument("-v", "--verbose", action="store_true",
                   help="Print more information when program is running.")

    # Validate arguments
    args = p.parse_args()
    if args.T < 1:
        raise argparse.ArgumentError("Minimum number of tiles is 1.")
    if args.iter < 1:
        raise argparse.ArgumentError("Minimum number of iterations is 1.")
    if args.burnin < 0:
        raise argparse.ArgumentError("Burn-in must be positive.")
    if args.thin < 0:
        raise argparse.ArgumentError("Thinning parameter must be positive")
    if args.burnin > args.iter:
        warnings.warn("Burn-in greater that max iterations. Setting burn-in to half of max iterations.")
        args.burnin = int(args.iter / 2)
    if args.thin > args.iter - args.burnin:
        warnings.warn("Thinning parameter too large. Setting to default ({}).".format(DEF_THIN))
        args.thin = DEF_THIN

    return args
