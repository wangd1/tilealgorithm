import numpy as np
from scipy.misc import logsumexp
from scipy.misc import comb
import pickle

class Model():
    def __init__(
            self,
            x,
            t,
            a=1,
            b=1
    ):
        self.x = x
        self.n = x[0].shape[1]  # Number of samples
        self.m = len(x)  # Number of features
        self.maxJ = x[0].shape[0]
        self.totalReads = [np.sum(r,axis=0) for r in x]
        self.totalReadsMat = np.stack(self.totalReads)
        print(self.totalReadsMat)
        self.s = np.stack([r[0,:] for r in x]) # Success
        self.f = np.stack([self.totalReads[r] - self.x[r][0, :] for r in range(self.m)])
        self.mask = np.zeros((self.m,self.maxJ))
        for i in range(self.m):
            n = np.sum(np.nansum(x[i],axis=1) > 0)
            self.mask[i,0:n] = 1
        print(self.mask)

        # Precompute combs due to run time

        self.com = []
        for i in range(self.maxJ):
            # Make s and f
            s = np.stack([r[i, :] for r in self.x])
            s[np.isnan(s)] = 0
            self.com.append(np.log(comb(self.totalReadsMat, s)))
        """
        N = 90
        M = 100
        x = np.random.binomial(1, 0.05, (M, N)).astype(float)
        #x[np.random.randint(0, M, 20), np.random.randint(0, N, 20)] = np.nan
        x[10:50, 10:50] = np.random.binomial(1, 0.9, (40, 40)).astype(float)
        #x[np.random.randint(10, 50, 400), np.random.randint(10, 50, 400)] = np.nan
        x[40:70, 50:80] = np.random.binomial(1, 0.5, (30, 30)).astype(float)
        #x[np.random.randint(40, 70, 10), np.random.randint(50, 80, 10)] = np.nan
        x[80:90, 80:90] = np.random.binomial(1, 0.9, (10, 10)).astype(float)
        #x[np.random.randint(80, 90, 70), np.random.randint(80, 90, 70)] = np.nan
        x[70:90, 20:40] = np.random.binomial(1, 0.90, (20, 20)).astype(float)
        #x[np.random.randint(70, 90, 30), np.random.randint(20, 40, 30)] = np.nan
        self.s = x
        self.f = 1-x
        """
        self.t = t # Number of tiles
        self.a = a # Alpha hyperparameter
        self.b = b # Beta hyperparameter

        self.pc = np.repeat(0.5,self.t) # column prior
        self.pr = np.repeat(0.5,self.t) # row prior

        # Initialize random starts
        self.r = np.zeros((self.m,self.t)) # Row labels
        self.r[range(self.m),np.random.randint(0,self.t,self.m)] = 1
        self.c = np.zeros((self.n,self.t)) # Column labels
        self.c[range(self.n),np.random.randint(0,self.t,self.n)] = 1
        self.p = np.zeros(self.t + 1) # Tile value parameter
        self.mp = np.zeros(self.t + 1) # Tile missing value parameter
        self.ll = [np.zeros((self.m,self.n)) for i in range(self.t + 1)] # Likelihood mats
        self.pdist = None
        self.mpdist = None

    def sampleR(self):
        # Compute prev labels
        proposal = self.r.copy()#np.random.binomial(1,0.5,(self.m,self.t))
        #proposal = np.abs(self.r - proposal)
        p = 0.5
        r = np.random.rand(self.m)
        gen_trunc = np.floor(np.log(1 - r * (1 - (1 - p) ** self.t)) / np.log(1 - p)) + 1
        for i,x in enumerate(gen_trunc):
            idx = np.random.choice(self.t,int(x),replace=False)
            proposal[i,idx] = 1 - proposal[i,idx]

        prev = [np.outer(self.r[:,i],self.c[:,i]) for i in range(self.t)]
        cur = [np.outer(proposal[:,i],self.c[:,i]) for i in range(self.t)]
        prevlabs = np.sum(prev, axis=0)
        curlabs = np.sum(cur, axis=0)
        remove = np.any(curlabs > 1, axis=1)
        prevll = (1-prevlabs) * self.ll[0]
        curll = (1-curlabs) * self.ll[0]
        for i in range(self.t):
            prevll += self.ll[i+1] * prev[i]
            curll += self.ll[i+1] * cur[i]
        priorcur = np.log(self.pr * proposal + (1-self.pr) * (1-proposal))
        priorprev = np.log(self.pr * self.r + (1-self.pr) * (1-self.r))
        ratio = np.minimum(0,(np.sum(curll,axis=1) + np.sum(priorcur,axis=1)) - (np.sum(prevll,axis=1) + np.sum(priorprev,axis=1)))
        ratio[remove] = -np.inf
        u = np.log(np.random.uniform(size=len(ratio)))
        accept = np.where(u < ratio)[0]
        self.r[accept,:] = proposal[accept,:]

    def sampleC(self):
        #proposal = np.random.binomial(1, 0.5, (self.n, self.t))
        #proposal = np.abs(self.c - proposal)
        proposal = self.c.copy()
        p = 0.1
        r = np.random.rand(self.n)
        gen_trunc = np.floor(np.log(1 - r * (1 - (1 - p) ** self.t)) / np.log(1 - p)) + 1
        for i, x in enumerate(gen_trunc):
            idx = np.random.choice(self.t,int(x),replace=False)
            proposal[i, idx] = 1 - proposal[i, idx]

        prev = [np.outer(self.r[:, i], self.c[:, i]) for i in range(self.t)]
        cur = [np.outer(self.r[:, i], proposal[:, i]) for i in range(self.t)]
        prevlabs = np.sum(prev, axis=0)
        curlabs = np.sum(cur, axis=0)
        remove = np.any(curlabs > 1, axis=0)
        prevll = (1 - prevlabs) * self.ll[0]
        curll = (1 - curlabs) * self.ll[0]
        for i in range(self.t):
            prevll += self.ll[i + 1] * prev[i]
            curll += self.ll[i + 1] * cur[i]
        priorcur = np.log(self.pr * proposal + (1 - self.pr) * (1 - proposal))
        priorprev = np.log(self.pr * self.c + (1 - self.pr) * (1 - self.c))
        ratio = np.minimum(0, (np.sum(curll, axis=0) + np.sum(priorcur, axis=1)) - (np.sum(prevll, axis=0) + np.sum(priorprev, axis=1)))
        ratio[remove] = -np.inf
        u = np.log(np.random.uniform(size=len(ratio)))
        accept = np.where(u < ratio)[0]
        self.c[accept, :] = proposal[accept, :]

    def sampleP(self):
        tile_a = 0
        tile_b = 0
        tile_miss = 0
        tile_hit = 0
        alltiles = np.zeros((self.m,self.n))
        # Sample tile parameters
        for i in range(self.t):
            agree = np.outer(self.r[:,i],self.c[:,i])
            rows, cols = np.where(agree == 1)
            alpha = np.nansum(self.s[rows,cols])
            miss = np.sum(np.isnan(self.s[rows,cols]))
            beta = np.nansum(self.f[rows,cols])
            hit = len(self.s[rows,cols]) - miss
            self.p[i+1] = np.random.beta(self.a + alpha,self.b + beta)
            self.mp[i+1] = np.random.beta(self.a + miss,self.b + hit)
            tile_a += alpha
            tile_b += beta
            tile_miss += miss
            tile_hit += hit
            alltiles += agree

        # Sample background parameters
        #total_a = np.nansum(self.s)
        #total_b = np.nansum(self.f)
        #total_miss = np.sum(np.isnan(self.s))
        #alpha = total_a - tile_a
        #beta = total_b - tile_b
        #alpha_miss = total_miss - tile_miss
        #beta_miss = self.m * self.n - total_miss - tile_hit
        self.p[0] = 0.05#np.random.beta(self.a + alpha, self.b + beta)
        self.mp[0] = 0.05
        #rows, cols = np.where(alltiles == 0)
        #self.p[0] = np.random.choice(np.random.beta(self.a + self.s[rows,cols],self.b + self.f[rows,cols]))
        #self.mp[0] = np.random.beta(self.a + alpha_miss, self.b + beta_miss)
        # Compute likelihood matrices and store
        for i in range(self.t + 1):
            self.ll[i] = np.log(self.p[i]) * self.s + np.log(1-self.p[i]) * self.f
            missloc = np.isnan(self.s)
            self.ll[i][missloc] = np.log(self.mp[i])
            self.ll[i][~missloc] += np.log(1-self.mp[i])


    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.random.beta(10 + alpha,10 + self.n)

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.random.beta(10 + alpha,10 + self.m)

    def sampleJ(self):
        labels = [np.outer(self.r[:, i], self.c[:, i]) for i in range(self.t)]
        jll = []
        missloc = np.isnan(self.s)
        for i in range(self.maxJ):
            # Make s and f
            s = np.stack([r[i,:] for r in self.x])
            f = np.stack([self.totalReads[r] - self.x[r][i,:] for r in range(self.m)])
            for k in range(self.t + 1):
                self.ll[k] = np.log(self.p[k]) * s + np.log(1 - self.p[k]) * f + self.com[i]
                self.ll[k][missloc] = np.log(self.mp[k])
                self.ll[k][~missloc] += np.log(1 - self.mp[k])
            tmp = self.ll[0] * (1- np.sum(labels,axis=0))
            for j in range(self.t):
                tmp += self.ll[j+1] * labels[j]
            jll.append(np.sum(tmp,axis=1))
        jll = np.stack(jll,axis=1)
        # Mask here
        jll[np.where(self.mask==0)] = -np.inf
        probs = np.exp(jll.T - logsumexp(jll,axis=1))
        for i in range(self.m):
            flip = np.where(np.random.multinomial(1, probs[:,i]) == 1)[0]
            self.s[i,:] = self.x[i][flip,:]
            self.f[i,:] = self.totalReads[i] - self.x[i][flip,:]

    def inference(self, iter=30000,burnin=15000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            self.sampleP()
            self.sampleC()
            self.sampleR()
            self.samplePC()
            self.samplePR()
            self.sampleJ()
            if i % thin == 0 and i >= burnin:
                self.pdist[cursamp,:] = self.p
                self.mpdist[cursamp,:] = self.mp
                cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist






