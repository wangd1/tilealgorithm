import numpy as np
from scipy.misc import logsumexp
from scipy.misc import comb
from util import dbetabinom
import pickle

class Model():
    def __init__(
            self,
            x,
            t,
            a=1,
            b=1
    ):
        self.x = x # Data
        self.n = x.shape[2]  # Number of samples
        self.m = x.shape[1]  # Number of features
        self.maxJ = x.shape[0] # Maximum number of junctions
        self.totalReads = np.sum(self.x,axis=0) # Total reads
        self.s = self.x[0].copy() # Success matrix
        self.f = self.totalReads - self.s # Failure matrix
        self.miss = np.isnan(self.s).astype(int)
        self.nonmiss = 1 - self.miss
        print(np.nansum(self.s[10:40,40:70]))
        print(np.nansum(self.f[10:40,40:70]))

        # Precompute combs due to run time

        #self.com = np.zeros((self.maxJ, self.m, self.n))
        #for i in range(self.maxJ):
        #    self.com[i] = np.log(comb(self.totalReads, self.x[i]))
        #    self.com[i,np.nansum(x[i],axis=1) == 0,:] = -np.inf


        """
        N = 90
        M = 100
        x = np.random.binomial(1, 0.05, (M, N)).astype(float)
        #x[np.random.randint(0, M, 20), np.random.randint(0, N, 20)] = np.nan
        x[10:50, 10:50] = np.random.binomial(1, 0.9, (40, 40)).astype(float)
        #x[np.random.randint(10, 50, 400), np.random.randint(10, 50, 400)] = np.nan
        x[40:70, 50:80] = np.random.binomial(1, 0.5, (30, 30)).astype(float)
        #x[np.random.randint(40, 70, 10), np.random.randint(50, 80, 10)] = np.nan
        x[80:90, 80:90] = np.random.binomial(1, 0.9, (10, 10)).astype(float)
        #x[np.random.randint(80, 90, 70), np.random.randint(80, 90, 70)] = np.nan
        x[70:90, 20:40] = np.random.binomial(1, 0.90, (20, 20)).astype(float)
        #x[np.random.randint(70, 90, 30), np.random.randint(20, 40, 30)] = np.nan
        self.s = x
        self.f = 1-x
        """
        self.t = t # Number of tiles

        self.pc = np.repeat(0.5,self.t) # column prior
        self.pr = np.repeat(0.5,self.t) # row prior

        # Initialize random starts
        bounds_row = int(self.m / self.t)
        bounds_col = int(self.n/ self.t)
        row_idx = np.repeat(np.arange(self.t),bounds_row)
        col_idx = np.repeat(np.arange(self.t),bounds_col)
        self.r = np.zeros((self.m, self.t))  # Row labels
        self.c = np.zeros((self.n,self.t)) # Column labels
        self.r[np.arange(len(row_idx)),row_idx] = 1
        self.c[np.arange(len(col_idx)),col_idx] = 1
        self.a = np.array([1] + [np.nansum(np.outer(self.r[:, i], self.c[:, i]) * self.s) + 1for i in range(self.t)])
        self.b = np.array([1] + [np.nansum(np.outer(self.r[:, i], self.c[:, i]) * self.f) + 1for i in range(self.t)])
        self.miss_a = np.array([1] + [np.sum(np.outer(self.r[:, i], self.c[:, i]) * self.miss) + 1 for i in range(self.t)])
        self.miss_b = np.array([1] + [np.sum(np.outer(self.r[:, i], self.c[:, i]) * self.nonmiss) + 1 for i in range(self.t)])
        self.pdist = None
        self.mpdist = None

    def sampleR(self):
        # Compute prev labels
        proposal = self.r.copy()
        p = 0.5
        r = np.random.rand(self.m)
        gen_trunc = np.floor(np.log(1 - r * (1 - (1 - p) ** self.t)) / np.log(1 - p)) + 1
        for i,x in enumerate(gen_trunc):
            idx = np.random.choice(self.t,int(x),replace=False)
            proposal[i,idx] = 1 - proposal[i,idx]

        row_labels_cur = [np.outer(proposal[:,i],self.c[:,i]) * (i+1) for i in range(self.t)]
        row_labels_prev = [np.outer(self.r[:,i],self.c[:,i]) * (i+1) for i in range(self.t)]
        constraint = np.where(np.any(np.count_nonzero(row_labels_cur,axis=0) > 1,axis=1) == False)[0]
        alpha_cur = np.zeros((self.m,self.t))
        beta_cur = np.zeros((self.m,self.t))
        alpha_prev = np.zeros((self.m,self.t))
        beta_prev = np.zeros((self.m,self.t))
        miss_alpha_cur = np.zeros((self.m,self.t))
        miss_beta_cur = np.zeros((self.m,self.t))
        miss_alpha_prev = np.zeros((self.m,self.t))
        miss_beta_prev = np.zeros((self.m,self.t))
        for i in range(self.t):
            alpha_cur[:,i] = np.nansum(row_labels_cur[i] / (i+1) * self.s,axis=1)
            beta_cur[:,i] = np.nansum(row_labels_cur[i] / (i+1) * self.f,axis=1)
            alpha_prev[:,i] = np.nansum(row_labels_prev[i] / (i+1) * self.s,axis=1)
            beta_prev[:,i] = np.nansum(row_labels_prev[i] / (i+1) * self.f,axis=1)
            miss_alpha_cur[:,i] = np.nansum(row_labels_cur[i] / (i+1) * self.miss,axis=1)
            miss_beta_cur[:,i] = np.nansum(row_labels_cur[i] / (i+1) * self.nonmiss,axis=1)
            miss_alpha_prev[:,i] = np.nansum(row_labels_prev[i] / (i + 1) * self.miss, axis=1)
            miss_beta_prev[:,i] = np.nansum(row_labels_prev[i] / (i + 1) * self.nonmiss, axis=1)

        row_labels_cur = np.sum(row_labels_cur,axis=0).astype(int)
        row_labels_prev = np.sum(row_labels_prev,axis=0).astype(int)

        for i in constraint:
            #self.a[1:] -= alpha_prev[i,:]
            #self.b[1:] -= beta_prev[i,:]
            #self.miss_a[1:] -= miss_alpha_prev[i,:]
            #self.miss_b[1:] -= miss_beta_prev[i,:]
            prevll = np.nansum(dbetabinom(self.totalReads[i,:],self.s[i,:],self.a[row_labels_prev[i,:]],self.b[row_labels_prev[i,:]]))
            prevmissll = np.sum(dbetabinom(1,self.miss[i,:],self.miss_a[row_labels_prev[i,:]],self.miss_b[row_labels_prev[i,:]]))
            self.a[1:] -= alpha_prev[i,:]
            self.b[1:] -= beta_prev[i,:]
            self.a[1:] += alpha_cur[i, :]
            self.b[1:] += beta_cur[i, :]
            self.miss_a[1:] -= miss_alpha_prev[i,:]
            self.miss_b[1:] -= miss_beta_prev[i,:]
            self.miss_a[1:] += miss_alpha_cur[i, :]
            self.miss_b[1:] += miss_beta_cur[i, :]
            curll = np.nansum(dbetabinom(self.totalReads[i,:],self.s[i,:],self.a[row_labels_cur[i,:]],self.b[row_labels_cur[i,:]]))
            #prevmissll = np.sum(dbetabinom(1,self.miss[i,:],self.miss_a[row_labels_prev[i,:]],self.miss_b[row_labels_prev[i,:]]))
            curmissll = np.sum(dbetabinom(1,self.miss[i,:],self.miss_a[row_labels_cur[i,:]],self.miss_b[row_labels_cur[i,:]]))
            ratio = np.minimum(0,curll + curmissll - prevll - prevmissll)
            #ratio = np.minimum(0, curll - prevll)
            u = np.log(np.random.uniform(0,1))
            if u < ratio:
                self.r[i,:] = proposal[i,:]
                #self.a[1:] += alpha_cur[i,:]
                #self.b[1:] += beta_cur[i,:]
                #self.miss_a[1:] += miss_alpha_cur[i,:]
                #self.miss_b[1:] += miss_beta_cur[i,:]
            else:
            #    self.a[1:] += alpha_prev[i,:]
            #    self.b[1:] += beta_prev[i,:]
            #    self.miss_a[1:] += miss_alpha_prev[i,:]
            #    self.miss_b[1:] += miss_beta_prev[i,:]
                self.a[1:] += alpha_prev[i, :]
                self.b[1:] += beta_prev[i, :]
                self.a[1:] -= alpha_cur[i, :]
                self.b[1:] -= beta_cur[i, :]
                self.miss_a[1:] += miss_alpha_prev[i,:]
                self.miss_b[1:] += miss_beta_prev[i,:]
                self.miss_a[1:] -= miss_alpha_cur[i,:]
                self.miss_b[1:] -= miss_beta_cur[i,:]

    def sampleC(self):
        proposal = self.c.copy()
        p = 0.5
        r = np.random.rand(self.n)
        gen_trunc = np.floor(np.log(1 - r * (1 - (1 - p) ** self.t)) / np.log(1 - p)) + 1
        for i, x in enumerate(gen_trunc):
            idx = np.random.choice(self.t,int(x),replace=False)
            proposal[i, idx] = 1 - proposal[i, idx]

        col_labels_cur = [np.outer(self.r[:,i],proposal[:,i]) * (i+1) for i in range(self.t)]
        col_labels_prev = [np.outer(self.r[:,i],self.c[:,i]) * (i+1) for i in range(self.t)]
        constraint = np.where(np.any(np.count_nonzero(col_labels_cur,axis=0) > 1,axis=0) == False)[0]
        alpha_cur = np.zeros((self.n,self.t))
        beta_cur = np.zeros((self.n,self.t))
        alpha_prev = np.zeros((self.n,self.t))
        beta_prev = np.zeros((self.n,self.t))
        miss_alpha_cur = np.zeros((self.n,self.t))
        miss_beta_cur = np.zeros((self.n,self.t))
        miss_alpha_prev = np.zeros((self.n,self.t))
        miss_beta_prev = np.zeros((self.n,self.t))

        for i in range(self.t):
            alpha_cur[:,i] = np.nansum(col_labels_cur[i] / (i+1) * self.s,axis=0)
            beta_cur[:,i] = np.nansum(col_labels_cur[i] / (i+1) * self.f,axis=0)
            alpha_prev[:,i] = np.nansum(col_labels_prev[i] / (i+1) * self.s,axis=0)
            beta_prev[:,i] = np.nansum(col_labels_prev[i] / (i+1) * self.f,axis=0)
            miss_alpha_cur[:,i] = np.nansum(col_labels_cur[i] / (i+1) * self.miss,axis=0)
            miss_beta_cur[:,i] = np.nansum(col_labels_cur[i] / (i+1) * self.nonmiss,axis=0)
            miss_alpha_prev[:,i] = np.nansum(col_labels_prev[i] / (i + 1) * self.miss, axis=0)
            miss_beta_prev[:,i] = np.nansum(col_labels_prev[i] / (i + 1) * self.nonmiss, axis=0)

        col_labels_cur = np.sum(col_labels_cur,axis=0).astype(int)
        col_labels_prev = np.sum(col_labels_prev,axis=0).astype(int)
        for i in constraint:
            #self.a[1:] -= alpha_prev[i,:]
            #self.b[1:] -= beta_prev[i,:]
            #self.miss_a[1:] -= miss_alpha_prev[i, :]
            #self.miss_b[1:] -= miss_beta_prev[i, :]
            prevll = np.nansum(dbetabinom(self.totalReads[:,i],self.s[:,i],self.a[col_labels_prev[:,i]],self.b[col_labels_prev[:,i]]))
            prevmissll = np.sum(dbetabinom(1, self.miss[:,i], self.miss_a[col_labels_prev[:,i]], self.miss_b[col_labels_prev[:,i]]))
            self.a[1:] -= alpha_prev[i,:]
            self.b[1:] -= beta_prev[i,:]
            self.a[1:] += alpha_cur[i, :]
            self.b[1:] += beta_cur[i, :]
            self.miss_a[1:] -= miss_alpha_prev[i,:]
            self.miss_b[1:] -= miss_beta_prev[i,:]
            self.miss_a[1:] += miss_alpha_cur[i, :]
            self.miss_b[1:] += miss_beta_cur[i, :]
            curll = np.nansum(dbetabinom(self.totalReads[:,i],self.s[:,i],self.a[col_labels_cur[:,i]],self.b[col_labels_cur[:,i]]))
            #prevmissll = np.sum(dbetabinom(1, self.miss[:,i], self.miss_a[col_labels_prev[:,i]], self.miss_b[col_labels_prev[:,i]]))
            curmissll = np.sum(dbetabinom(1, self.miss[:,i], self.miss_a[col_labels_cur[:,i]], self.miss_b[col_labels_cur[:,i]]))

            ratio = np.minimum(0,curll + curmissll - prevll - prevmissll)
            #ratio = np.minimum(0, curll - prevll)

            u = np.log(np.random.uniform(0,1))
            if u < ratio:
                self.c[i,:] = proposal[i,:]
                #self.a[1:] += alpha_cur[i,:]
                #self.b[1:] += beta_cur[i,:]
                #self.miss_a[1:] += miss_alpha_cur[i,:]
                #self.miss_b[1:] += miss_beta_cur[i,:]
            else:
                #self.a[1:] += alpha_prev[i,:]
                #self.b[1:] += beta_prev[i,:]
                #self.miss_a[1:] += miss_alpha_prev[i,:]
                #self.miss_b[1:] += miss_beta_prev[i,:]
                self.a[1:] += alpha_prev[i, :]
                self.b[1:] += beta_prev[i, :]
                self.a[1:] -= alpha_cur[i, :]
                self.b[1:] -= beta_cur[i, :]
                self.miss_a[1:] += miss_alpha_prev[i,:]
                self.miss_b[1:] += miss_beta_prev[i,:]
                self.miss_a[1:] -= miss_alpha_cur[i,:]
                self.miss_b[1:] -= miss_beta_cur[i,:]

    def sampleP(self):
        # Sample tile parameters
        for i in range(self.t + 1):
            if i == 0:
                self.ll[i] = dbetabinom(self.totalReads,self.s,0.5,0.5)
            else:
                agree = np.outer(self.r[:,i-1],self.c[:,i-1])
                rows, cols = np.where(agree == 1)
                alpha = np.nansum(self.s[rows,cols])
                #miss = np.sum(np.isnan(self.s[rows,cols]))
                beta = np.nansum(self.f[rows,cols])
                #hit = len(self.s[rows,cols]) - miss
                #self.ll[i] = np.log(self.p[i]) * self.s + np.log(1-self.p[i]) * self.f
                self.ll[i] = dbetabinom(self.totalReads,self.s,0.5+alpha,0.5+beta)
            #self.p[i+1] = np.random.beta(self.a + alpha,self.b + beta)
            #self.mp[i+1] = np.random.beta(self.a + miss,self.b + hit)

        #self.p[0] = 0.05
        #self.mp[0] = 0.05

        # Compute likelihood matrices and store
        for i in range(self.t + 1):

            missloc = np.isnan(self.s)
            #self.ll[i][missloc] = np.log(self.mp[i])
            #self.ll[i][~missloc] += np.log(1-self.mp[i])


    def samplePC(self):
        alpha = np.sum(self.c,axis=0)
        self.pc = np.random.beta(10 + alpha,10 + self.n)

    def samplePR(self):
        alpha = np.sum(self.r, axis=0)
        self.pr = np.random.beta(10 + alpha,10 + self.m)

    def sampleJ(self):
        labels = [np.outer(self.r[:, i], self.c[:, i]).astype(int) * (i+1) for i in range(self.t)]
        clabels = np.sum(labels,axis=0)
        for i in range(self.m):
            for j in range(self.t):
                self.a[j+1] -= np.nansum(labels[j][i,:] / (j+1) * self.s[i,:])
                self.b[j+1] -= np.nansum(labels[j][i,:] / (j+1) * self.f[i,:])
            ll = np.zeros(self.maxJ)
            for k in range(self.maxJ):
                if np.nansum(self.x[k,i,:]) > 0:
                    ll[k] = np.nansum(dbetabinom(self.totalReads[i,:],self.x[k,i,:],self.a[clabels[i,:]],self.b[clabels[i,:]]))
                else:
                    ll[k] = -np.inf
            probs = np.exp(ll - logsumexp(ll))
            flip = np.where(np.random.multinomial(1,probs))[0]
            self.s[i,:] = self.x[flip,i,:]
            self.f[i,:] = self.totalReads[i,:] - self.x[flip,i,:]
            for j in range(self.t):
                self.a[j+1] += np.nansum(labels[j][i,:] / (j+1) * self.s[i,:])
                self.b[j+1] += np.nansum(labels[j][i,:] / (j+1) * self.f[i,:])

    def inference(self, iter=30000,burnin=15000,thin=100):
        total = int((iter - burnin) / thin)
        self.pdist = np.zeros((total,self.t + 1))
        self.mpdist = np.zeros((total,self.t + 1))
        cursamp = 0
        for i in range(iter):
            if i % 1000 == 0:
                print(i)
            #self.sampleP()
            self.sampleC()
            self.sampleR()
            #elf.samplePC()
            #self.samplePR()
            #self.sampleJ()
            #if i % thin == 0 and i >= burnin:
                #self.pdist[cursamp,:] = self.p
                #self.mpdist[cursamp,:] = self.mp
                #cursamp += 1


    def getTiles(self):
        out = np.zeros((self.m, self.n))
        for i in range(self.t):
            out = out + np.outer(self.r[:, i], self.c[:, i]) * (i + 1)
        return out

    def getTileParameters(self):
        return self.p

    def getTileSize(self):
        return np.exp(self.pr), np.exp(self.pc)

    def getPosterior(self):
        return self.pdist, self.mpdist







