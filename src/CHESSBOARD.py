from Model import Model
from Argparse import cmdline_args
import pickle
import numpy as np

if __name__ == "__main__":
    args = cmdline_args()

    with open(args.data, "rb") as data:
        m = Model(x=pickle.load(data,encoding='latin1'),t=args.T)
        m.inference(iter=args.iter, burnin=args.burnin, thin=args.thin)

        np.savetxt("test.txt", m.getTiles(), fmt='%i', delimiter=" ")

        with open(args.output + ".pkl","wb") as model:
            pickle.dump(m,model)

        labs = [np.outer(m.r[:,i],m.c[:,i]) for i in range(m.t)]

        for i in range(m.t):
            print(i)
            print(np.nansum(labs[i] * m.s))
            print(np.nansum(labs[i] * m.f))
